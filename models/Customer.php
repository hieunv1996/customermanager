<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Customer".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Address
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Address'], 'required'],
            [['Name'], 'string', 'max' => 100],
            [['Address'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'Customer ID',
            'Name' => 'Name',
            'Address' => 'Address',
        ];
    }
}
