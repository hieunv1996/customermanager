-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 12, 2017 at 09:19 AM
-- Server version: 5.7.18-0ubuntu0.16.04.1
-- PHP Version: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `CustomerManager`
--

-- --------------------------------------------------------

--
-- Table structure for table `Customer`
--

CREATE TABLE `Customer` (
  `ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Address` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Customer`
--

INSERT INTO `Customer` (`ID`, `Name`, `Address`) VALUES
(2, 'Nguyễn Văn Hiếu', 'Hà Nội'),
(3, 'Nguyễn Văn Hiếu', 'Hà Nội'),
(4, 'Nguyễn Văn Hiếu', 'Hà Nội'),
(5, 'Nguyễn Văn Hiếu', 'Hà Nội'),
(6, 'Nguyễn Văn Hiếu', 'Hà Nội'),
(7, 'Nguyễn Thị Vân Giang', 'Nghệ An'),
(9, 'Nguyễn Văn Hiếu', 'Hà Nội'),
(10, 'Nguyễn Văn Hiếu', 'Hà Nội'),
(11, 'Nguyễn Văn Hiếu', 'Hà Nội'),
(12, 'Nguyễn Văn Hiếu', 'Hà Nội'),
(13, 'Nguyễn Văn Hiếu', 'Hà Nội'),
(14, 'Nguyễn Văn Hiếu', 'Hà Nội'),
(15, 'Nguyễn Văn Hiếu', 'Hà Nội'),
(16, 'Nguyễn Văn Hiếu', 'Hà Nội'),
(17, 'Nguyễn Văn Hiếu', 'Hà Nội');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Customer`
--
ALTER TABLE `Customer`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Customer`
--
ALTER TABLE `Customer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
